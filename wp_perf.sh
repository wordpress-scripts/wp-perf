#!/bin/bash

. load_test.conf

if [ -f stop ]; then
  rm stop
fi

check_config

start_test() {
  test_conc=5
    for ((con_cache=1; con_cache<=$test_conc; con_cache++)); do
      load_landing_page $con_cache
    done

  create_del_post
}

start_test > log
